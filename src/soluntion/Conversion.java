package solution;

public abstract class Conversion {
	abstract public double convert(double value);
}
