package solution2;

public interface Conversion {
	public double convert(double value);
}
