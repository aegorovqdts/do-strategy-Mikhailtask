# do-strategy-Mikhailtask
Дополнительное задание и пример от Михаила

#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "Converter build and run",
            "command": "javac -sourcepath /projects/do-strategy-Mikhailtask/src -d /projects/do-strategy-Mikhailtask/bin /projects/do-strategy-Mikhailtask/src/*.java && java -classpath /projects/do-strategy-Mikhailtask/bin Converter",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/do-strategy-Mikhailtask/src",
                "component": "maven"
            }
        }
    ]
}
