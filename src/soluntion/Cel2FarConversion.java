package solution;

public class Cel2FarConversion extends Conversion {
	public double convert(double value) {
		return value*9/5 + 32;
	}
}
